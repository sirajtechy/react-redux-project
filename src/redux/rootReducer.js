import {combineReducers} from 'redux-immutable'
import uploadReducer from '../reducers/uploadReducer'

const rootReducer = combineReducers ({
    uploadReducer
})
export default rootReducer
